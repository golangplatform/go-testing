package message
import "testing"

// For black box testing
// package message_test
// with message_test - depart() will not be available here

// Pluralsight - Creating Well-tested Applications in Go
// Simple test case

func TestGreet(t *testing.T) {
got:= Greet("Gopher")
expect:="Hello, Gopher\n"
if got != expect{
	t.Errorf("Did not get expected result, Wanted %q, got %q\n",expect,got)
}
}

func TestDepart(t *testing.T){
	got:= depart("Gopher")
expect:="Good bye, Gopher\n"
if got != expect{
	t.Errorf("Did not get expected result, Wanted %q, got %q\n",expect,got)
}

}

// Table driven testing to check more scenarios
func TestTableDrivenTestCase(t *testing.T) {

	scenarios := []struct{
		input string
		output string

	} {
		{input:"Gopher", output:"Hello, Gopher\n"},
		{input:"", output:"Hello, \n"},
	}

	for _, s := range scenarios {
		got:= Greet(s.input)

		if got!=s.output {
			t.Errorf("Did not get expected result, Wanted %q, got %q\n",s.output,got)
		}
	}
}