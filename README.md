# go-testing

# Pluralsight course - Creating Well-tested Applications in Go

# 1. Useful commands
- go help test
- go test -> To execute text file present at current location
- go test message -> To execute test files present at message package
- go test -v -> To get verbose/valuable output
- go test -run Greet -> It focuses on perticular test. any test that matches regular expression will be executed with -run flag
- go test -cover -> To get details about what is to be covered during test execution(Test coverage report) . It is expensive than -run.
- go test -coverprofile cover.out -> To get more details about what is covered and what is not covered. A cover.out file is generated.
- go tool cover -func cover.out -> Used to analyze cover.out file and output on terminal
- go tool cover -html cover.out -> Used to analyze cover.out file and output as html on web browser
- go test -coverprofile count.out -covermode count -> To get mode graphical result
- go tool cover -html count.out -> To get mode graphical result
- go test -bench -> It runs all test, including benchmark test
- go test -bench -benchtime 10s -> Run benchmark test targeting the specified time. default benchtime is 1 sec
- go test -bench . -> To benchmark all tests 
- go test -benchmem -> Report memory allocation statistics for benchmarks
- go test -trace trace.out -> Record execution trace to trace.out file for analysis
- go test -{type}profile {file} -> Generate profile of requested type(block,cpu,mem,cover,mutex etc.)
- go test -bench . -memprofile profile.out
- go tool pprof profile.out