package benchmarksha

import (
	"testing"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
)
// Benchmark SHA hashing algorithm
func BenchmarkSHA1(b *testing.B) {
	data := []byte("Rohit P. Bhilare")
b.StartTimer()
	for r := 0; r < b.N ; r++ {
		sha1.Sum(data)
	}

}

// Benchmark SHA hashing algorithm
func BenchmarkSHA256(b *testing.B) {
	data := []byte("Rohit P. Bhilare")
b.StartTimer()
	for r := 0; r < b.N ; r++ {
		sha256.Sum256(data)
	}

}

// Benchmark SHA512 hashing algorithm
func BenchmarkSHA512(b *testing.B) {
	data := []byte("Rohit P. Bhilare")
b.StartTimer()
	for r := 0; r < b.N ; r++ {
		sha512.Sum512(data)
	}

}

// Benchmark SHA512 hashing algorithm
// Here we allocate memory and we run it with -benchmem flag to see changes in memory allocation as compared to BenchmarkSHA512 func
func BenchmarkSHA512Alloc(b *testing.B) {
	data := []byte("Rohit P. Bhilare")
b.StartTimer()
	for r := 0; r < b.N ; r++ {
		h := sha512.New()
		h.Sum(data)
	}

}